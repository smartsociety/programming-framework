/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.smartsocietyproject.scenario2.helper;

/**
 *
 * @author Svetoslav Videnov <s.videnov@dsg.tuwien.ac.at>
 */
public class JsonPeer {
    private String name;
    private String channelType;
    private String channel;
    private String role;

    public String getName() {
        return name;
    }

    public String getChannelType() {
        return channelType;
    }

    public String getChannel() {
        return channel;
    }

    public String getRole() {
        return role;
    }
    
}
